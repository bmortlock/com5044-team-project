USE fitnessapp_db;

DELETE FROM Activity WHERE id > 0;
DELETE FROM User WHERE id > 0;
DELETE FROM SelectedActivity WHERE user_id > 0;
DELETE FROM SelectedCustom WHERE id > 0;
DELETE FROM MoodSubmission WHERE id > 0;
DELETE FROM FitnessAttended WHERE id > 0;
DELETE FROM StudyAttended WHERE id > 0;

ALTER TABLE Activity AUTO_INCREMENT = 1;
ALTER TABLE User AUTO_INCREMENT = 1;
ALTER TABLE SelectedCustom AUTO_INCREMENT = 1;
ALTER TABLE MoodSubmission AUTO_INCREMENT = 1;
ALTER TABLE FitnessAttended AUTO_INCREMENT = 1;
ALTER TABLE StudyAttended AUTO_INCREMENT = 1;

INSERT INTO Activity (activity_name,location,weekday,event_start,event_end,female_only,staff,img_url,booking_url) VALUES
	("Badminton", "Sports Hall", "Monday", "780", "840", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6203/"),
    ("Table Tennis", "Sports Hall", "Monday", "780", "840", "0", NULL, NULL,"mailto:s.todd@leedstrinity.ac.uk"),
    ("Beginners Squash", "Squash Courts", "Monday", "720", "780", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6333/"),
    ("6-a-Side Football", "3G Pitch", "Monday", "1020", "1080", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6273/"),
    ("Tagged", "3G Pitch", "Monday", "1020", "1080", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6313/"),
    ("Athletics", "Running Track", "Monday", "1080", "1140", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6193/"),
    ("Squash", "Squash Courts", "Tuesday", "750", "840", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6333/"),
    ("Running", "Meet at Trinity Fitness", "Tuesday", "1050", "1050", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6193/"),
    ("Beginners Track Running", "Running Track", "Wednesday", "720", "720", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6193/"),
    ("Social Squash", "Squash Courts", "Wednesday", "960", "1020", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6333/"),
    ("Athletics", "Running Track", "Wednesday", "1080", "1140", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6193/"),
    ("Badminton", "Sports Hall", "Thursday", "1020", "1080", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6203/"),
    ("Free Play Squash", "Squash Courts", "Thursday", "1020", "1065", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6333/"),
    ("Basketball", "Sports Hall", "Thursday", "1080", "1140", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6213/"),
    ("Futsal", "Sports Hall", "Thursday", "1140", "1200", "0", NULL, NULL,"https://www.ltsu.co.uk/organisation/6273/"),
    ("Golf Driving Range", "Driving Range", "Thursday", "1140", "1200", "0", NULL, NULL,"mailto:s.todd@leedstrinity.ac.uk"),
	("Netball", "Sports Hall", "Monday", "1035", "1035", "1", NULL, NULL,"https://www.ltsu.co.uk/organisation/6293/"),
	("Beginner Running", "Meet at Trinity Fitness", "Wednesday", "795", "840", "1", NULL, NULL,"https://www.ltsu.co.uk/organisation/6193/"),
    ("Squash", "Squash Courts", "Thursday", "780", "840", "1", NULL, NULL,"mhttps://www.ltsu.co.uk/organisation/6333/"),
    ("Rugby", "Rugby Pitch", "Friday", "720", "780", "1", NULL, NULL,"https://www.ltsu.co.uk/organisation/6323/"),
	("Gym Circuit", "Fitness Suite", "Monday", "630", "660", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Studio Cycling", "Spin Studio", "Monday", "795", "825", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Beginners Pilates", "Gymnasium", "Monday", "1050", "1110", "0", "Sara", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Body Conditioning", "Gymnasium", "Monday", "1110", "1155", "0", "Elishea", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Studio Cycling", "Spin Studio", "Monday", "1125", "1185", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Metafit", "Gymnasium", "Tuesday", "435", "480", "0", "Charlotte", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Body Conditioning", "Gymnasium", "Tuesday", "795", "825", "0", "Elishea", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Intermediate Pilates", "Gymnasium", "Tuesday", "1050", "1110", "0", "Angela", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Zumba", "Gymnasium", "Tuesday", "1110", "1155", "0", "Louise", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Studio Cycling", "Spin Studio", "Tuesday", "1110", "1155", "0", "Anthony", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Kettlebells", "Gymnasium", "Tuesday", "1155", "1200", "0", "Charlotte", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Gym Circuit", "Fitness Suite", "Wednesday", "630", "660", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Studio Cycling", "Spin Studio", "Wednesday", "795", "825", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Fitness Pilates", "Gymnasium", "Wednesday", "1080", "1125", "0", "Johnny", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Studio Cycling", "Spin Studio", "Wednesday", "1110", "1155", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Metafit/MetaPWR", "Gymnasium", "Wednesday", "1125", "1160", "0", "James", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Metafit/MetaPWR", "Gymnasium", "Wednesday", "1165", "1200", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Gym Circuit", "Fitness Suite", "Thursday", "630", "660", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Kettlebells", "Gymnasium", "Thursday", "795", "825", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Iyengar Yoga", "Gymnasium", "Thursday", "1080", "1140", "0", "Helen", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("HIIT", "Gymnasium", "Thursday", "1155", "1200", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Studio Cycling", "Spin Studio", "Friday", "435", "465", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Gym Circuit", "Fitness Suite", "Friday", "735", "765", "0", NULL, NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("Pilates", "Gymnasium", "Friday", "780", "825", "0", "Charlotte", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login"),
    ("HIITSTEP", "Gymnasium", "Friday", "1050", "1095", "0", "James", NULL,"https://leedstrinity.legendonlineservices.co.uk/enterprise/account/Login");
    
INSERT INTO User (email,name,password,last_mood_submit,mood_notify,upcoming_notify,recommend_notify) VALUES
	("dannybrewer@yahoo.co.uk","Danny Brewer","danny0123","2018-11-11 13:23:44","0","1","1"),
	("saraaah@hotmail.com","Sarah Horley","sirensong12","2018-12-20 17:26:55","1","1","1"),
	("jerrymanders@hotmail.com","Jerry Manders","jjpow3r","2019-02-15 07:03:47","1","0","1"),
	("dfordoris@hotmail.com","Doris Day","Marblez05","2019-06-03 09:53:29","1","0","0"),
	("marshallmate@gmail.com","Marshall Mathers","marr48marr","2020-01-17 02:33:01","0","0","0");
    
INSERT INTO MoodSubmission (user_id,entry_date,mood_rating) VALUES
	(1,"2018-11-11 13:23:44","4"),
    (2,"2018-12-20 17:26:55","2"),
    (3,"2019-02-15 07:03:47","3"),
    (5,"2020-01-17 02:33:01","1"),
	(4,"2019-06-03 09:53:29","4");
    
INSERT INTO FitnessAttended (user_id,entry_date,attended) VALUES
	(1,"2018-11-11 13:23:44","1"),
    (2,"2018-12-20 17:26:55","1"),
    (3,"2019-02-15 07:03:47","0"),
    (5,"2020-01-17 02:33:01","0"),
	(4,"2019-06-03 09:53:29","0");

INSERT INTO StudyAttended (user_id,entry_date,attended) VALUES
	(1,"2018-11-11 13:23:44","4"),
    (2,"2018-12-20 17:26:55","2"),
    (3,"2019-02-15 07:03:47","3"),
    (5,"2020-01-17 02:33:01","1"),
	(4,"2019-06-03 09:53:29","4");
  
INSERT INTO SelectedActivity (user_id,activity_id) VALUES
	(2,1),
    (3,6),
    (5,7),
    (4,4),
	(1,2);

INSERT INTO SelectedCustom (user_id,session_name,study_session,location,weekday,event_start,event_end) VALUES
	(2,"COM5014 - Software Dev","1","LC1-28","Wednesday","600","690"),
    (2,"Run Around Campus","0","LTU Campus","Saturday","1140","1170"),
	(1,"calves and quads day!!","0","Gym","Friday","1020","1065");

    
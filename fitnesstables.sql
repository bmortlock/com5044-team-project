DROP DATABASE fitnessapp_db;
CREATE DATABASE fitnessapp_db;
USE fitnessapp_db;
DROP TABLE IF EXISTS Activity,User,SelectedActivity,SelectedCustom,MoodSubmission,FitnessAttended,StudyAttended;

CREATE TABLE Activity (
	id INT AUTO_INCREMENT,
	activity_name VARCHAR(30) NOT NULL,
    location VARCHAR(30) NOT NULL,
	weekday ENUM ('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
    event_start SMALLINT NOT NULL,
    event_end SMALLINT NOT NULL,
    female_only BOOLEAN NOT NULL,
    img_url VARCHAR(250) DEFAULT 'https://static.wixstatic.com/media/fe10d0_92572e77b9a24e179a2bb81c7baa935d~mv2.png',
    staff ENUM ('Fitness Staff','James','Helen','Charlotte','Louise','Johnny','Anthony','Angela','Elishea','Sara') DEFAULT 'Fitness Staff',
    booking_url VARCHAR(250) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE User (
	id INT AUTO_INCREMENT,
	email VARCHAR(45) UNIQUE NOT NULL,
	name VARCHAR(25) UNIQUE NOT NULL,
	password VARCHAR(25) NOT NULL,
    last_mood_submit DATETIME,
    mood_notify BOOLEAN DEFAULT '1',
    upcoming_notify BOOLEAN DEFAULT '1',
    recommend_notify BOOLEAN DEFAULT '1',
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE MoodSubmission (
	id INT AUTO_INCREMENT,
	user_id INT NOT NULL,
    entry_date DATETIME NOT NULL,
    mood_rating ENUM ('1','2','3','4','5') NOT NULL,
    PRIMARY KEY (id,user_id),
    FOREIGN KEY (user_id) REFERENCES User (id)
) ENGINE=InnoDB;

CREATE TABLE FitnessAttended (
	id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
	entry_date DATETIME NOT NULL,
    attended BOOLEAN NOT NULL,
    PRIMARY KEY (id,user_id),
    FOREIGN KEY (user_id) REFERENCES User (id)
) ENGINE=InnoDB;

CREATE TABLE StudyAttended (
	id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
	entry_date DATETIME NOT NULL,
    attended BOOLEAN NOT NULL,
    PRIMARY KEY (id,user_id),
    FOREIGN KEY (user_id) REFERENCES User (id)
) ENGINE=InnoDB;

CREATE TABLE SelectedActivity (
    user_id INT NOT NULL,
    activity_id INT NOT NULL,
	PRIMARY KEY (user_id, activity_id),
    FOREIGN KEY (user_id) REFERENCES User (id),
    FOREIGN KEY (activity_id) REFERENCES Activity (id)
) ENGINE=InnoDB;

CREATE TABLE SelectedCustom (
    id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    session_name VARCHAR(30) NOT NULL,
    study_session BOOLEAN NOT NULL,
    location VARCHAR(30) NOT NULL,
    weekday ENUM ('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
    event_start SMALLINT NOT NULL,
    event_end SMALLINT NOT NULL,
	PRIMARY KEY (id,user_id),
    FOREIGN KEY (user_id) REFERENCES User (id)
) ENGINE=InnoDB;

package main;

public class Activity {
	private int id;
	private String activity_name;
	private String location;
	private String weekday;
	public Activity()
	{
	}
	public Activity(int id,String activity_name,String location,String weekday)
	{
	this.id=id;
	this.activity_name=activity_name;
	this.location=location;
	this.weekday=weekday;
	}
	
	public void setId(int id) {
	this.id = id;
	}
	public void setActivity_name(String activity_name) {
	this.activity_name = activity_name;
	}
	public void setLocation(String location) {
	this.location = location;
	}
	public void setWeekday(String weekday) {
	this.weekday = weekday;
	}
	
	public int getId() {
	return id;
	}
	public String getLocation() {
	return location;
	}
	public String getActivity_name() {
	return activity_name;
	}
	public String getWeekday() {
	return weekday;
	}
	}
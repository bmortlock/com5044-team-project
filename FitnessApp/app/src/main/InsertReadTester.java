package main;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import java.util.List;

public class MainActivity extends ActionBarActivity {

@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);

DBHandler db = new DBHandler(this);

Log.d(�Insert: �, �Inserting sample entries...�);
db.addActivity(new Activity(�Football�, �Astroturf�));
db.addShop(new Shop(�Basketball�, �Courts�));

Log.d(�Reading: �, �Reading all activities...�);
List<Activity> activities = db.getAllActivities();

for (Activity activity : activities) {
String log = �Id: � + activity.getId() + � ,Activity name: � + shop.getActivity_name() + � ,Location: � + shop.getLocation();
Log.d(�Activity: : �, log);
}
}
}
package main;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

private static final int DATABASE_VERSION = 1;
private static final String DATABASE_NAME = �fitnessapp�;
private static final String TABLE_ACTIVITY = �activity�;
private static final String KEY_ID = �id�;
private static final String KEY_ACTIVITY_NAME = �activity_name�;
private static final String KEY_LOCATION = �location�;
private static final String KEY_WEEKDAY = �weekday�;
public DBHandler(Context context) {
super(context, DATABASE_NAME, null, DATABASE_VERSION);
}

@Override
public void onCreate(SQLiteDatabase db) {
String CREATE_ACTIVITY_TABLE = �CREATE TABLE � + TABLE_ACTIVITY + �(�
+ KEY_ID + � INTEGER PRIMARY KEY,� + KEY_ACTIVTY_NAME + � TEXT,�
+ KEY_LOCATION + � TEXT,� + KEY_WEEKDAY + � TEXT�
+ �)�;
db.execSQL(CREATE_ACTIVITY_TABLE);
}

@Override
public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
db.execSQL(�DROP TABLE IF EXISTS � + TABLE_ACTIVITY);
onCreate(db);
}

public void addActivity(Activity activity) {
SQLiteDatabase db = this.getWritableDatabase();
ContentValues values = new ContentValues();
values.put(KEY_ACTIVITY_NAME, activity.getActivity_name());
values.put(KEY_LOCATION, activity.getLocation());
db.insert(TABLE_ACTIVITY, null, values);
db.close();
}

public List<Activity> getAllActivities() {
List<Activity> activityList = new ArrayList<Activity>();
String selectQuery = "SELECT * FROM " + TABLE_ACTIVITY;
SQLiteDatabase db = this.getWritableDatabase();
Cursor cursor = db.rawQuery(selectQuery, null);
if (cursor.moveToFirst()) {
do {
Activity activity = new Activity();
activity.setId(Integer.parseInt(cursor.getString(0)));
activity.setActivity_name(cursor.getString(1));
activity.setLocation(cursor.getString(2));
activityList.add(activity);
} while (cursor.moveToNext());
}
return activityList;
}
}
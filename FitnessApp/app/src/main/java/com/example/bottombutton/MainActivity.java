package com.example.bottombutton;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpTab();

    }

    private void setUpTab(){
        TabLayout tabLayout = findViewById(R.id.tabb);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.colorBottom));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.home_fitness));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.home_mentalhealth));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.home_study));

        ImageView imgView = new ImageView(MainActivity.this);
        imgView.setImageResource(R.drawable.home_mentalhealth);
        imgView.setPadding(20,20,20,20);
        tabLayout.getTabAt(0).setCustomView(imgView);

        imgView = new ImageView(MainActivity.this);
        imgView.setImageResource(R.drawable.home_fitness);
        imgView.setPadding(20,20,20,20);
        tabLayout.getTabAt(1).setCustomView(imgView);

        imgView = new ImageView(MainActivity.this);
        imgView.setImageResource(R.drawable.home_study);
        imgView.setPadding(20,20,20,20);
        tabLayout.getTabAt(2).setCustomView(imgView);
    }
}

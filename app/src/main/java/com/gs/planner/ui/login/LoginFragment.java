package com.gs.planner.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.gs.planner.BaseActivity;
import com.gs.planner.DBHelper;
import com.gs.planner.MainActivity;
import com.gs.planner.R;
import com.gs.planner.ui.home.HomeFragment;

public class LoginFragment extends Fragment {
    private static final String TAG = LoginFragment.class.getSimpleName();
    private BaseActivity activityListener;

    //private Button submit, cancel;
    DBHelper db;
    Button bLogin , cancel;
    EditText etUsername, etEmail, etPPassword, etPassword;
    Session session;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        etEmail = (EditText) rootView.findViewById(R.id.etEmailRegister);
        etPassword = (EditText) rootView.findViewById(R.id.etPasswordRegister);
        bLogin = (Button) rootView.findViewById(R.id.btnSubmitLogin);
        cancel = (Button) rootView.findViewById(R.id.btnQuitLogin);
        db = new DBHelper(getContext());
        session = new Session(getContext());


        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Code For login using Shared preferences
                String userEmail = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                if(db.getUser(userEmail,password)){
                    session.setLoggedin(true);
                    startActivity(new Intent(activityListener.getApplicationContext(), MainActivity.class));
                    activityListener.finish();


                  // HomeFragment homeFragment = new HomeFragment();
                    //FragmentManager fragmentManager=getFragmentManager();

                   // fragmentManager.beginTransaction().replace(R.id.frameLayoutRegister, new AccountMethodFragment()).commit();

                }else{
                    Toast.makeText(getContext(), "Wrong email/password",Toast.LENGTH_SHORT).show();
                }




            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Code to call signup fragment

                LoginFragment loginFragment = new LoginFragment();
                FragmentManager fragmentManager= getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frameLayoutLogin, new AccountMethodFragment()).commit();

            }
        });


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            activityListener = (BaseActivity) getActivity();
        } catch (Exception e) {
            Log.e(TAG, "Activity must extend BaseActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityListener = null;
    }


}

